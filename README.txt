// $Id$

Magento Check / Money order
-------------------------------------------------------------------------------

Provides ability to pay orders via `Check / Money order` Magento Payment method.

This module implements Magento Cart Quote hooks to integrate `Check / Money order`
payment method into checkout process in Drupal. You can use code of this module as
a start point to create your own Payment method.

For detailed Instructions on Integrating payment module into Magento Cart Quote 
please see https://prj.adyax.com/projects/drupal-magento/wiki/Payment_modules.

INSTALLATION
-------------------------------------------------------------------------------

Install as usual, see http://drupal.org/node/70151 for further information.
After enabling module you will have to activate this payment method as
"Enabled payment system" option on Magento Cart Quote settings form at
admin/settings/magento_cart_quote/settings.

VERY IMPORTANT NOTES
-------------------------------------------------------------------------------

By default Magento Cart Quote settings form used for managing payment methods is 
compartible with domain module and allows to set different settings for each domain.
So if you use domain module for separate site domains, you will need to edit other
domain settings to enable this payment method on the other domains. 

OTHER WARNINGS
-------------------------------------------------------------------------------

You have to configure and enable the "Check / Money order" Payment Method in Magento
to make this Payment method work correctly in Drupal.

Maintainers
-------------------------------------------------------------------------------
Module is fully developpped, sponsored and maintained by Adyax.

Current maintainer: Ivan Tsekhmistro - itsekhmistro@adyax.com
